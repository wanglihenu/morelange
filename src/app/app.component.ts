import { Component ,OnInit} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, from} from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'welcome';
  constructor(private translate:TranslateService){
		//添加语言支持
    translate.addLangs(["en", "zn","fa"]);
    //设置默认语言，一般在无法匹配的时候使用
    translate.setDefaultLang('zn');
    //获取当前浏览器环境的语言比如en、 zn
    let browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|zn|fa/) ? browserLang : 'zn');

  }
  //切换语言
  changeLang(lang) {
	  this.translate.use(lang);
	  //当前选择的语言
	 	//console.log(this.translate.getBrowserLang());
	  //当前浏览器的语言环境
    //console.log(this.translate.getBrowserCultureLang());
	}
//setStyle() {
//	  return {
//	    'font-size': '22px',
//	    'color': 'blue'
//	  }
//}
  
}

 
